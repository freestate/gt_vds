import Vue from "vue"
import Router from "vue-router"
import Index from "@/templates/Index"
import NotFound from "@/templates/NotFound"

import OffersPage from "@/templates/OffersPage"
import AssociatePage from "@/templates/AssociatePage"
import ContactPage from "@/templates/ContactPage"
import ImprintPage from "@/templates/ImprintPage"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "Index",
      component: Index,
    },
    {
      path: "/angebot",
      name: "OffersPage",
      component: OffersPage,
    },
    {
      path: "/partner",
      name: "AssociatePage",
      component: AssociatePage,
    },
    {
      path: "/kontakt",
      name: "ContactPage",
      component: ContactPage,
    },
    {
      path: "/impressum",
      name: "ImprintPage",
      component: ImprintPage,
    },
    {
      path: "*",
      name: "NotFound",
      component: NotFound,
    },
  ],
})
